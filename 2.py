# Патрин Е.С.(ЗКИ19-16Б) Вариант-15 Работа №2
try:
    mass1 = []
    K = 7    # Кол-во столбцов
    N = 7    # Кол-во строк
    sumColomns = []
    for k in range(K):
        sumColomns.append(0)                     
    for k in range(K):
        mass2 = []
        sumLines = 0
        for n in range(N):
            if n + 2 > k:
                WoodalNumber = n*(k**n)-1        # Заполнение по формуле Вудала
                sumLines += WoodalNumber         # Подсчет суммы строки
                mass2.append(WoodalNumber)
                sumColomns[n] += mass2[n]        #Подсчет суммы столбцов
            else:
                mass2.append(0)
        mass2.append(sumLines)
        mass1.append(mass2)
    sumColomns.append(0)            # Пересечение суммы строк и столбцов равно 0
    mass1.append(sumColomns)        # Добавление последней строки содержащей сумму всех столбцов
    for n in range(N+1):  
        print(str(mass1[n]))
except:
    print("Ошибка")