﻿# Патрин Е.С.(ЗКИ19-16Б) Вариант-15 Работа №1
from PIL import Image, ImageDraw
try:
    def from_k_to_bin(k: int) -> list:
        k //= 17
        binary = bin(k)[2:]
        binary = ("0" * (1802 - len(binary))) + binary
        lists = [[] for x in range(17)]
        for x in range(1802):
            lists[-(x % 17)].append(binary[x])
        # Создаем изображение
        image = Image.new("1", (106,17), (0)) # Создаем черно-белое изображение 106х17
        draw = image.load()
        for y in range(17):
            for x in range(106):
                image.putpixel(xy=(105-x,16-y), value=(int(lists[y][x]),)) # Каждый пиксель окрашиваем в цвет, который хранится в двумерном списке lists
        image.save("image.png") # Сохраняем изображение
    from_k_to_bin(4858450636189713423582095962494202044581400587983244549483093085061934704708809928450644769865524364849997247024915119110411605739177407856919754326571855442057210445735883681829823754139634338225199452191651284348332905131193199953502413758765239264874613394906870130562295813219481113685339535565290850023875092856892694555974281546386510730049106723058933586052544096664351265349363643957125565695936815184334857605266940161251266951421550539554519153785457525756590740540157929001765967965480064427829131488548259914721248506352686630476300)
except:
    print("Ошибка")