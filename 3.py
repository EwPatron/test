﻿# Патрин Е.С.(ЗКИ19-16Б) Вариант-15 Работа №3
import random
try:
    RandMin = 0        # Минимальное число в интервале
    RandMax = 5        # Максимальное число в интервале
    lengthTuple = 5    # Длина кортежей
    a = ([random.randint(RandMin, RandMax) for _ in range(lengthTuple)])    #Заполняем кортеж рандомным числом
    b = ([random.randint(RandMin, RandMax) for _ in range(lengthTuple)])
    countMass1 = 0
    countMass2 = 0
    countAll = 0
    equallyMass1 = []    # Массив эллементов, содержащий различные со вторым кортежом элементы
    equallyMass2 = []
    for i in range(lengthTuple):
        for j in range(lengthTuple):
            if a[i] == b[j]:
                countMass1+=1
                break
            if a[i] not in equallyMass1 and j == lengthTuple-1:
                countAll += 1
                equallyMass1.append(a[i])
    for i in range(lengthTuple):
        for j in range(lengthTuple):
            if b[i] == a[j]:
                countMass2+=1
                break
            if b[i] not in equallyMass2 and j == lengthTuple-1:
                countAll += 1
                equallyMass2.append(b[i])
    print("Кортеж 1: "+ str(a))
    print("Кортеж 2: "+ str(b))
    print("Задание а:")
    print("Кол-во одинаковых чисел в первом кортеже: "+str(countMass1))
    print("Кол-во одинаковых чисел во втором кортеже: "+str(countMass2))
    print("Задание б:")
    print("Кол-во разных чисел в первом кортеже: "+str(lengthTuple-countMass1))
    print("Кол-во разных чисел во втором кортеже: "+str(lengthTuple-countMass2))
    print("Задание в:")
    print("Общее кол-во уникальных чисел: "+str(countAll))
    print("Задание г:")
    print("Числа, входящие только в первый кортеж: "+str(equallyMass1))
    print("Числа, входящие только во второй кортеж: "+str(equallyMass2))
except:
    print("Ошибка в файле")
