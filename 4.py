import numpy as np


def algorithm(p, graph, i, j):
    """Поиск кратчайших маршрутов между городами i и j в списке городов p"""
    try:
        i,j = int(i), int(j)
        distance = 0
        if(i==j):
            print (i,)
        elif(p[i,j] == -30000):
            print (i,'-',j)
        else:
            distance = distance + graph[i,j]
            algorithm(p, graph, i, p[i,j]);
            print(j,)
        return distance
    except:
        print("Ошибка в функции algorithm")


def newCities(graph):
    """Создание массива городов"""
    try:
        v = len(graph)
        p = np.zeros(graph.shape)
        for i in range(0,v):
            for j in range(0,v):
                p[i,j] = i
                if (i != j and graph[i,j] == 0):
                    p[i,j] = -30000
                    graph[i,j] = 30000
        for k in range(0,v):
            for i in range(0,v):
                for j in range(0,v):
                    if graph[i,j] > graph[i,k] + graph[k,j]:
                        graph[i,j] = graph[i,k] + graph[k,j]
                        p[i,j] = p[k,j]
        return p
    except:
        print("Ошибка в функции newCities")
        if graph.__len__() != 6:
            print("Кол-во растояний между городами меньше или больше 6")


def input():
    """Ввод растояния между городами с клавиатуры"""
    try:
        graph = np.array([[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]])
        for i in range(6):
            print(
                "Введите растояния между 6 городами(через пробел), если города не соеденены или это текущий город, пишите 0:")
            while True:
                mass = [int(el) for el in input().split()]
                if mass.__len__() == 6:
                    break
                else:
                    print("ВНИМАНИЕ! Введено неверное количество городов, попробуйте еще раз:")
            graph[i] = mass
            mass.clear()
        return graph
    except:
        print("Ошибка в функции input")


def test_1():
    graph = np.array([[0, 3, 5, 0, 0, 25], [0, 0, 0, 7, 0, 12], [0, 0, 0, 0, 10, 0], [0, 0, 0, 0, 0, 8], [0, 0, 0, 0, 0, 5], [0, 0, 0, 0, 0, 0]])
    assert algorithm(newCities(graph), graph, 0, 5) == 15


def test_2():
    graph = np.array([[5, 1, 2, 4, 8, 25], [3, 5, 6, 7, 8, 12], [10, 0, 12, 0, -5, 0], [100, 0, 0, 50, 0, 0], [0, 0, 0, 0, 0, 0], [10, 10, 10, 10, 10, 10]])
    assert algorithm(newCities(graph), graph, 0, 5) == 13


def test_3():
    graph = np.array([[-1, -3, -5, -10, -5, -25], [0, 0, -4, -5, -6, -12], [0, 0, 0, 0, -8, -6], [0, 0, 0, 0, 0, -5], [0, 0, 0, 0, 0, -5], [0, 0, 0, 0, 0, 0]])
    assert algorithm(newCities(graph), graph, 0, 5) == -27


def test_4():
    graph = np.array([[0, 0, 0, 6, 5, 25], [0, 0, 0, 0, 0, 3], [0, 0, 0, 0, 0, 7], [0, 9, 8, 0, 0, 8], [0, 19, 2, 0, 0, 5], [0, 0, 0, 0, 0, 0]])
    assert algorithm(newCities(graph), graph, 0, 5) == 10


def test_5():
    graph = np.array([[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]])
    assert algorithm(newCities(graph), graph, 0, 5) == 0