# Патрин Е.С.(ЗКИ19-16Б) Вариант-15 Работа №5
# Шифрование документа txt формата
def encryption(wayFile):      
    f = open(wayFile,'r')
    try:
        # Шифруются только те буквы текста, что есть в словаре alphabet
        alphabet = {' ':0,'q':1, 'w':2, 'e':3, 'r':4, 't':5, 'y':6, 'u':7, 'i':8,'o':9, 'p':10,
        'a':11, 's':12, 'd':13, 'f':14, 'g':15, 'h':16, 'j':17, 'k':18,'l':19,
        'z':20, 'x':21, 'c':22, 'v':23, 'b':24, 'n':25, 'm':26}        # Соответствие буквы к цифре
        alphabetTwo = {0:' ', 1:'q', 2:'w', 3:'e', 4:'r', 5:'t', 6:'y', 7:'u', 8:'i',9:'o', 10:'p',
        11:'a', 12:'s', 13:'d', 14:'f', 15:'g', 16:'h', 17:'j', 18:'k',19:'l',
        20:'z', 21:'x', 22:'c', 23:'v', 24:'b', 25:'n', 26:'m'}
        key = 'welcome'        # Ключ-слово
        keymass = []           # Массив цифр ключ-слова
        textmass = []          # Массив цифр текста документа
        text = f.read()
        for i in key:
            if i in alphabet.keys():
                keymass.append(alphabet[i])
        for i in text:
            if i in alphabet.keys():
                textmass.append(alphabet[i])
        for i in range(len(textmass)):       # Шифровка Виженера, изменяем массив цифр текста документа
            k = i%len(key)
            textmass[i] += keymass[k]
            if textmass[i] > len(alphabet)-1:
                textmass[i] -= len(alphabet)-1
        f.close()
        f = open(wayFile,'w')
        chipr = ''
        for i in range(len(textmass)):
            chipr += alphabetTwo[textmass[i]]
        f.writelines(str(key)+'\n')
        f.writelines(chipr)
        f.close()
        print("Шифрование завершено")
    except:
        print("Ошибка")
    finally:
        f.close()

# Дешифровка документа txt формата
def decipherment(wayFile):
    f = open(wayFile,'r')
    try:
        # Дешифруются только те буквы текста, что есть в словаре alphabet
        alphabet = {' ':0,'q':1, 'w':2, 'e':3, 'r':4, 't':5, 'y':6, 'u':7, 'i':8,'o':9, 'p':10,
        'a':11, 's':12, 'd':13, 'f':14, 'g':15, 'h':16, 'j':17, 'k':18,'l':19,
        'z':20, 'x':21, 'c':22, 'v':23, 'b':24, 'n':25, 'm':26}        # Соответствие буквы к цифре
        alphabetTwo = {0:' ', 1:'q', 2:'w', 3:'e', 4:'r', 5:'t', 6:'y', 7:'u', 8:'i',9:'o', 10:'p',
        11:'a', 12:'s', 13:'d', 14:'f', 15:'g', 16:'h', 17:'j', 18:'k',19:'l',
        20:'z', 21:'x', 22:'c', 23:'v', 24:'b', 25:'n', 26:'m'}
        key = f.readline()     # Ключ-слово
        keymass = []           # Массив цифр ключ-слова
        textmass = []          # Массив цифр текста документа
        text = f.read()
        for i in key:
            if i in alphabet.keys():
                keymass.append(alphabet[i])
        for i in text:
            if i in alphabet.keys():
                textmass.append(alphabet[i])
        for i in range(len(textmass)):       # Дешифровка шифра Виженера, изменяем массив цифр текста документа
            k = i%(len(key)-1)
            textmass[i] = textmass[i] - keymass[k]
            if textmass[i]<0:
                textmass[i] += len(alphabet)-1
        f.close()
        f = open(wayFile,'w')
        chipr = ''
        for i in range(len(textmass)):
            chipr += alphabetTwo[textmass[i]]
        f.writelines(chipr)
        f.close()
        print("Дешифрование завершено")
    except:
        print("Ошибка")
    finally:
        f.close()
encryption('specimen.txt')
decipherment('specimen.txt')

